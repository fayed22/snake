var size;
var BackGroundWidth,BackGroundHeight;
var snakeHeadX,snakeHeadY;
var SnakeArray = null; 
var SnakeFood = null; 
var spriteBackGround = null; 
var BackGroundPos;
var Direction ="";
var scl=0.10;
var charecter;
var food = 0,coin=0,snk=0; 
var jumpup=9,jumpdown=3,jumpleft=12,jumpright=0; 
var cellWidth = 30,cellHeight=30;
var Enum = {snakecell:0, snakefood:1 , background:2,snakecoin:3,snakecharecter:4,bomb:5};
var snakeJSGameLayer = null;
var snakeLength = 3; //Length of the snake
var ScoreLabel = null;
var GameOverLabel = null;
var bCollisionDetected = false;
var p=0,q=0;
var r,b;
var that;
var flag=true;
var bombSprite,bombCreated=false;
var startTime,endTime,touchTime;

//var SnakeCharecter;
// var x1=[170,200,230,260,290,320,350];[410,440,470,500,530,560,590,620,650,680,710,740];
// var y1=[320,290,260,230,200,170,140];[160,170,180,190,200,210,220,230,240,250,260,270];
// var x2=[740,710,680,650,620,590,560];
// var y2=[293,316,339,362,385,408,431];

var SpriteBackGround = cc.Sprite.extend({
 ctor: function(texture, rect) 
 {           
    this._super(texture, rect);
 }
});
//Snake cell class 
var SpriteSnakeCell = cc.Sprite.extend({
    ctor: function(texture) 
    {           
        this._super(texture);
    }
});
//Snake food class 
var SpriteSnakeFood = cc.Sprite.extend({
    ctor: function(texture) {           
    this._super(texture);
 }
});

var SpriteSnakeBomb = cc.Sprite.extend({
    ctor: function(texture) {           
    this._super(texture);
 }
});

var HelloWorldLayer = cc.Layer.extend({
    sprite:null,
    ctor:function () {
        this._super();
        size= cc.winSize;
        that=this;
        //var pinfo = cc.autopolygon.generatePolygon(res.Background);
        spriteBackGround= new cc.Sprite.create(res.Background);
        spriteBackGround.setAnchorPoint(0,0);
        spriteBackGround.setScale(0.8);
        spriteBackGround.setTag(Enum.background);
        // BackGroundWidth = spriteBackGround.getBoundingBox().width;
        // BackGroundHeight = spriteBackGround.getBoundingBox().height;
        spriteBackGround.x = 0;//(size.width - BackGroundWidth)/2;
        spriteBackGround.y = 0;//(size.height - BackGroundHeight)/2;
        BackGroundPos = {x:spriteBackGround.x, y:spriteBackGround.y};
        this.addChild(spriteBackGround);

        GameOverLabel = new cc.LabelTTF("Game Over!!!!!", "Arial", 38); 
        GameOverLabel.x = size.width / 2;
        GameOverLabel.y = size.height / 2;  
        GameOverLabel.setColor(255,0,0);
        this.addChild(GameOverLabel, 5); 
        GameOverLabel.visible = false;

        FoodLabel= new cc.LabelTTF("Food : 0","Arial",32);
        FoodLabel.x=size.width/4;
        FoodLabel.y=size.height/2+150;
        FoodLabel.setColor(0,0,0);
        this.addChild(FoodLabel,5);
        FoodLabel.visible=true;

        CoinLabel= new cc.LabelTTF("Coin : 0","Arial",32);
        CoinLabel.x=size.width/2+200;
        CoinLabel.y=size.height/2+150;
        CoinLabel.setColor(0,0,0);
        this.addChild(CoinLabel,5);
        CoinLabel.visible=true;


        d=1;
        this.createSnake();
        //this.createCoin();
        this.createFood();
        this.createCharacter();
        this.schedule(this.CallForBomb,15);
        if ('keyboard' in cc.sys.capabilities) {
            cc.eventManager.addListener({
                event: cc.EventListener.KEYBOARD,
                onKeyPressed: function (key, event) {
                   var target = event.getCurrentTarget();
                   if(key == "37" && direction != "right") direction = "left";
                   else if(key == "38" && direction != "down") direction = "up";
                   else if(key == "39" && direction != "left") direction = "right";
                   else if(key == "40" && direction != "up") direction = "down";
                 } 
            }, this);
        } else {
            cc.log("KEYBOARD Not supported");
        }
        if(cc.sys.capabilities.hasOwnProperty('mouse'))
        {
            cc.eventManager.addListener({
                event:cc.EventListener.MOUSE,
                onMouseDown:function(event)
                    {
                        var x=event.getLocationX();
                        var y=event.getLocationY();
                        cc.log(x,y);
                    }

                
                
            },this); 
        }

        this.schedule(this.gameLoop,1);
        return true;
    },
    
    createSnake:function()
    {
        direction="down";
        if(( typeof SnakeArray != 'undefined' && SnakeArray instanceof Array ) && SnakeArray.length > 0 )
        {                
            for(var i = 0; i< SnakeArray.length; i++)             
            {
                this.removeChild(SnakeArray[i],true);                   
            }
        } 
        SnakeArray = [];   
        var elmsToRemove = SnakeArray.length - length;
        if(elmsToRemove>1)
        {
            SnakeArray.splice(snakeLength-1,elmsToRemove);
        }

        for(var i = snakeLength-1; i>=0; i--)
        {
           
           var spriteSnakeCell = new SpriteSnakeCell("res/d3.png");
           spriteSnakeCell.setScale(0.05);
           spriteSnakeCell.setAnchorPoint(0,0);
           spriteSnakeCell.setTag(Enum.snakecell);          
           var xMov = (i*cellWidth)+BackGroundPos.x+80-(q);
           var yMov = (spriteBackGround.y)+220+(p);
           p+=30;
           q+=10
           //cc.log(xMov,yMov);
           spriteSnakeCell.x = xMov;
           spriteSnakeCell.y = yMov;
           this.addChild(spriteSnakeCell,2);           
           SnakeArray.push(spriteSnakeCell); 
        }
        
    },
    createFood:function() {   
        //Check if food Exist , remove it from the game sprite
        r=this.getRandomArbitrary(1,3);
        cc.log(r);
        if(this.getChildByTag(Enum.snakefood)!=null)
        {
            this.removeChildByTag(Enum.snakefood,true); 
        }       
        var spriteSnakeFood = new SpriteSnakeFood("res/p"+r+".png");
        //spriteSnakeFood.setAnchorPoint(0,0);
        spriteSnakeFood.setScale(0.20);
        spriteSnakeFood.setTag(Enum.snakefood);     
        this.addChild(spriteSnakeFood,3); 
        var rndValX = 0;
        var rndValY = 0;
        var minX = 410; 
        var minY =200;
        var maxWidth = 520;
        var maxHeight = 350;
        var multiple = 10;
        //Place it in some random position 
        rndValX = generate(minX,maxWidth,multiple);
        cc.log(rndValX);
        //cc.log(maxWidth,maxHeight);
        rndValY = generate(minY,maxHeight,multiple);
        cc.log(rndValY);
        //cc.log(rndValX,rndValY);
        var irndX = rndValX+BackGroundPos.x;
        var irndY = rndValY+BackGroundPos.y;
        SnakeFood = {
            x: irndX , 
            y: irndY  
        };
       spriteSnakeFood.x = SnakeFood.x; 
       spriteSnakeFood.y = SnakeFood.y; 
    },
    CallForBomb:function()
    {
        this.createBomb();
        setTimeout(function(){
            bombSprite.x=0;
            bombSprite.y=0;
            cc.log("tag of bomb "+bombSprite.getTag());
            that.removeChildByTag(bombSprite.getTag(),true);
        },7000);
    },
    createBomb:function()
    {
        
        b=0;
        bombCreated=true;
        bomb=true;
        var p=new Date();
        startTime=p.getSeconds();
        endTime=startTime+7;
        if(endTime>60)
        {
            endTime=endTime-60;
        }
        cc.log("start "+startTime);
        cc.log("End "+(startTime+7));
        if(this.getChildByTag(Enum.bomb)!=null)
        {
            this.removeChildByTag(Enum.bomb,true); 
        } 
        bombSprite= new SpriteSnakeBomb("res/bomb.png");
        bombSprite.setTag(Enum.bomb);
        bombSprite.setScale(0.40);
        bombSprite.visible=true;
        this.addChild(bombSprite);
        var rndValX = 0;
        var rndValY = 0;
        var minX = 410; 
        var minY =200;
        var maxWidth = 520;
        var maxHeight = 350;
        var multiple = 10;
        //Place it in some random position 
        rndValX = generate(minX,maxWidth,multiple);
        cc.log(rndValX);
        //cc.log(maxWidth,maxHeight);
        rndValY = generate(minY,maxHeight,multiple);
        cc.log(rndValY);
        //cc.log(rndValX,rndValY);
        var irndX = rndValX+BackGroundPos.x;
        var irndY = rndValY+BackGroundPos.y;
        Bomb = {
            x: irndX , 
            y: irndY  
        };
        bombSprite.x=Bomb.x;
        bombSprite.y=Bomb.y;

    },
    BombCount:function()
    {
        //if(bomb)
        //cc.log("endtime "+endTime+" tch "+touchTime);
        var m=((endTime-touchTime)*1000);
        cc.log("diff b2wn create and time "+m);
        {
            b++;
            cc.log("b ",b);
        }
        setTimeout(function(){
            //bombCreated=false;
            that.unscheduleCount();
            
            //bomb=false;
        },m);
    },
    unscheduleCount:function()
    {
        cc.log("length"+SnakeArray.length);
        if(SnakeArray.length>=b)
        {
            for(var j=b;j<=SnakeArray.length;j++)
            {
                deletSpriteSnakeCell=SnakeArray.pop();
                this.removeChildByTag(deletSpriteSnakeCell.getTag(),true);
                //SnakeArray[j].removeChildByTag(spriteSnakeCell.getTag(),true);
            }
        }
        if(SnakeArray.length<3)
        {
            this.pause();
            GameOverLabel.setVisible(true);
            return;
        }
        this.unschedule(this.BombCount);
    },
    createCharacter:function()
    {
        //charecter=true;
        if(this.getChildByTag(Enum.snakecharecter)!=null)
        {
            this.removeChildByTag(Enum.snakecharecter,true); 
        }       
        var spriteSnakeCharecter = new SpriteSnakeFood("res/dragon.png");
        //spriteSnakeFood.setAnchorPoint(0,0);
        spriteSnakeCharecter.setScale(0.25);
        //spriteSnakeCharecter.setTag(Enum.)
        spriteSnakeCharecter.setTag(Enum.snakecharecter);     
        this.addChild(spriteSnakeCharecter,3); 
        var rndValX = 0;
        var rndValY = 0;
        var minX = 410; 
        var minY =100;
        var maxWidth = 520;
        var maxHeight = 450;
        var multiple = cellWidth;
        //Place it in some random position 
        rndValX = generate(minX,maxWidth,multiple);
        //cc.log(maxWidth,maxHeight);
        rndValY = generate(minY,maxHeight,multiple);
        //cc.log(rndValX,rndValY);
        var irndX = rndValX+BackGroundPos.x;
        var irndY = rndValY+BackGroundPos.y;
        SnakeCharecter = {
            x: irndX , 
            y: irndY  
        };
     
       spriteSnakeCharecter.x = SnakeCharecter.x; 
       spriteSnakeCharecter.y = SnakeCharecter.y;
    },
    gameLoop:function()
    {
        SnakeHeadX = SnakeArray[0].x;
        SnakeHeadY = SnakeArray[0].y;
        //cc.log(SnakeHeadX.SnakeHeadY);
        switch(direction)
        {
            case "right":
                SnakeHeadX+=cellWidth;
                SnakeHeadY+=(cellWidth-20);
                d=1;
                jumpright++;
                jumpleft--;

            break;
            case "left":
                SnakeHeadX-=(cellWidth);
                SnakeHeadY-=(cellWidth-20);
                d=4;
                jumpleft++;
                jumpright--;
            break;
            case "up":
                SnakeHeadX-=(cellWidth);
                 SnakeHeadY+=(cellWidth-7);//7
                 
                 d=2;
                jumpup++;
                jumpdown--;
            break;
            case "down":
                  SnakeHeadX+=cellWidth;
                  SnakeHeadY-=cellWidth-7;
                  
                  d=3;
                  jumpdown++;
                  jumpup--;

            break;
            default:
                cc.log("direction not defind");
            
        }
        //cc.log("up "+jumpup+" down "+jumpdown+" left "+jumpleft+" right "+jumpright);
        if(CollisionDetector(SnakeHeadX,SnakeHeadY,SnakeArray))
        {
            //cc.direction.pause();
            this.pause();
            GameOverLabel.setVisible(true);
            //cc.log("col");
            return;
        }

        if(Math.abs(SnakeHeadX-SnakeCharecter.x)<35&&Math.abs(SnakeHeadY-SnakeCharecter.y)<35)
        {
            
            snk++;
            //cc.log("snake x "+SnakeHeadX+" Snake y "+SnakeHeadX+" char x "+SnakeCharecter.getPosition().x+" char y "+SnakeCharecter.getPosition().y);
            var spriteSnaketail = new SpriteSnakeCell("res/d"+d+".png");
            spriteSnaketail.setAnchorPoint(0,0);
            spriteSnaketail.setScale(0.05);
            spriteSnaketail.setTag(Enum.snakecell);
            this.addChild(spriteSnaketail,2);
            spriteSnaketail.x = SnakeHeadX;
            spriteSnaketail.y = SnakeHeadY;
            SnakeArray.push(spriteSnaketail);
            this.removeChildByTag(Enum.snakecharecter,true);
            cc.log(snk+"   "+SnakeArray.length);
            this.createCharacter();
            
            
        }
        if(bombCreated)
        {
            
            if(Math.abs(SnakeHeadX-bombSprite.x)<35&&Math.abs(SnakeHeadY-bombSprite.y)<35)
            {
                //this.BombCount();
                //if(bombTouched){
                bombCreated=false;
                //bombTouched=false;
                var p= new Date();
                touchTime=p.getSeconds();
                cc.log("touch "+touchTime);
                var spriteSnakeCellLast = SnakeArray.pop(); 
                var changeImageOfSnake=new SpriteSnakeCell("res/d"+d+".png");
                changeImageOfSnake.x = SnakeHeadX; 
                changeImageOfSnake.y = SnakeHeadY;
                //cc.log("change snake "+SnakeHeadX,SnakeHeadY)
                changeImageOfSnake.setScale(0.05);
                changeImageOfSnake.setAnchorPoint(0,0);
                this.addChild(changeImageOfSnake,2);
                SnakeArray.unshift(changeImageOfSnake);
                this.removeChildByTag(spriteSnakeCellLast.getTag(),true);
                this.schedule(this.BombCount,1);

                //}
            }
        }
        if(Math.abs(SnakeHeadX - SnakeFood.x)<35 && Math.abs(SnakeHeadY - SnakeFood.y)<35)
        {
                if(r==2)
                {
                    var w=this.getRandomArbitrary(1,5);
                    if(w==1)
                    {
                        food+=25;
                    }
                    else if(w==2)
                    {
                        food+=50;
                    }
                    else if(w==3)
                    {
                        food+=75;
                    }
                    else
                    {
                        food+=100;
                    }
                    FoodLabel.setString("Food : "+(food));
                }
                else
                {
                    var l=this.getRandomArbitrary(1,5);
                    if(l==1)
                    {
                        coin+=25;
                    }
                    else if(l==2)
                    {
                        coin+=50;
                    }
                    else if(l==3)
                    {
                        coin+=75;
                    }
                    else
                    {
                        coin+=100;
                    }
                    CoinLabel.setString("Coin : "+(coin));
                    
                }
                var spriteSnakeCellLast = SnakeArray.pop(); 
                var changeImageOfSnake=new SpriteSnakeCell("res/d"+d+".png");
                changeImageOfSnake.x = SnakeHeadX; 
                changeImageOfSnake.y = SnakeHeadY;
                //cc.log("change snake "+SnakeHeadX,SnakeHeadY)
                changeImageOfSnake.setScale(0.05);
                changeImageOfSnake.setAnchorPoint(0,0);
                this.removeChildByTag(Enum.snakefood,true);
                this.addChild(changeImageOfSnake,2);

                SnakeArray.unshift(changeImageOfSnake);

                this.removeChildByTag(spriteSnakeCellLast.getTag(),true);
                if(SnakeArray.length<5)
                {
                    this.createFood();
                }
                else
                {
                    this.createSecondFood();
                }
                
                
                // var q=this.getRandomArbitrary(1,3);
                //this.createFood();         
        }
        else
        {
            var spriteSnakeCellLast = SnakeArray.pop(); 
            var changeImageOfSnake=new SpriteSnakeCell("res/d"+d+".png");
            changeImageOfSnake.x = SnakeHeadX; 
            changeImageOfSnake.y = SnakeHeadY;
            //cc.log("change snake "+SnakeHeadX,SnakeHeadY)
            changeImageOfSnake.setScale(0.05);
            changeImageOfSnake.setAnchorPoint(0,0);
            this.addChild(changeImageOfSnake,2);
            SnakeArray.unshift(changeImageOfSnake);
            this.removeChildByTag(spriteSnakeCellLast.getTag(),true);
        }
    },
    
    createSecondFood:function()
    {
        
        if(flag)
        {
            flag=false;
            this.schedule(this.createFood,6);
        }
        setTimeout(function(){
            flag=true;
        },6000);
    },
    getRandomArbitrary:function(min,max) 
    {
        return Math.floor(Math.random() * (max - min) + min);
    }
    
});
function generate(min, max, multiple) 
{
    var res = Math.floor(Math.random() * ((max - min) / multiple)) * multiple + min;
    return res;
}

function CollisionDetector(snakeHeadX,snakeHeadY,snakeArray)
{
        if(jumpright<0||jumpright>12||jumpleft>12||jumpleft<0||jumpup>12||jumpup<0||jumpdown>12||jumpdown<0)
        {
            return true;
        }
        for(var i = 0; i < snakeArray.length; i++)
        {
            if(snakeArray[i].x == snakeHeadX && snakeArray[i].y == snakeHeadY)
            {
                return true;
            }
        }
        return false;
}
var HelloWorldScene = cc.Scene.extend({
    onEnter:function () {
        this._super();
        var layer = new HelloWorldLayer();
        this.addChild(layer);
    }
});

